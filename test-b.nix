{
  name = "Bestest integratoin test of the world (B)";

  nodes = {
    server = { config, ... }: {
      imports = [
        ./module.nix
      ];
      services.project_b.enable = true;
    };
  };

  testScript = { nodes, ... }: ''
    start_all()

    UNIX_SOCKET = "${nodes.server.services.project_b.unixSocketPath}"

    server.wait_for_unit("project_b.service")
    server.wait_for_open_unix_socket(UNIX_SOCKET)

    input_string  = "lol bamboozled"
    output = server.succeed(f"echo {input_string} | nc -U {UNIX_SOCKET}", timeout=3)
    assert input_string.upper() in output, "We receive the correct upper-cased input string"
  '';
}
