final: prev: {
  project_b = final.callPackage ./package.nix {};

  megacorp = prev.megacorp or {} // {
    tests = prev.megacorp.tests or {} // {
      test-b = final.testers.runNixOSTest ./test-b.nix;
    };
  };
}
