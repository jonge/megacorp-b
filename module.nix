{ config, pkgs, lib, ... }:
let
  cfg = config.services.project_b;
in
{
  options.services.project_b = {
    enable = lib.mkEnableOption "enable Project B service";
    unixSocketDir = lib.mkOption {
      type = lib.types.path;
      default = "/var/lib/project_b";
      description = "UNIX Socket directory of project B";
    };
    unixSocketName = lib.mkOption {
      type = lib.types.str;
      default = "project_b.sock";
      description = "UNIX Socket file name of project B";
    };
    unixSocketPath = lib.mkOption {
      type = lib.types.path;
      default = cfg.unixSocketDir + "/" + cfg.unixSocketName;
      description = "UNIX Socket full path of project B";
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.tmpfiles.rules = [
      "d ${cfg.unixSocketDir} 1777 root root"
    ];

    systemd.services.project_b = {
      description = "Best echo server in the world";
      wantedBy = [ "multi-user.target" ];
      serviceConfig.ExecStart = "${pkgs.project_b}/bin/project_b ${cfg.unixSocketPath}";
    };
  };
}
