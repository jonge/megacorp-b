let
  sources = import ./nix/sources.nix {};
  pkgs = import sources.nixpkgs {
    overlays = [
      (import ./overlay.nix)
    ];
  };

in

{
  inherit (pkgs) project_b;

  test-b = pkgs.testers.runNixOSTest ./test-b.nix;

  devShell = pkgs.mkShell {
    nativeBuildInputs = with pkgs; [
      niv
      python3
    ];
  };
}
