{ python3
, runCommand
}:

runCommand "project_b" { buildInputs = [ python3 ]; } ''
  mkdir -p $out/bin
  cp ${./src/main.py} $out/bin/project_b
  patchShebangs $out
''
