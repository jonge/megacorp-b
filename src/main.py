#!/usr/bin/env python3

import socket
import os, os.path
import time
from collections import deque
import sys

SOCKET = sys.argv[1]

if os.path.exists(SOCKET):
  os.remove(SOCKET)

server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
server.bind(SOCKET)

while True:
  server.listen(1)
  conn, addr = server.accept()
  try:
    datagram = conn.recv(1024)
    if datagram:
      conn.send(datagram.upper())
  finally:
    conn.close()
